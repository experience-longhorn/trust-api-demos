using MS.Internal.Privacy;
using MS.Internal.Security.Configuration;
using System;
using System.Security.Policy;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace LaunchTrustPages
{
	class Program
	{
		[STAThread]
		static void Main(string[] args)
		{
			Application avalonApp = new Application();

			Window avalonWnd = new Window();
			avalonWnd.Text = "Launch Consent";
			avalonWnd.EnterView += new EnterViewEventHandler(LaunchTrust);
			avalonWnd.Show();

			avalonApp.Run();
		}

		static void LaunchTrust(Element target, EnterViewEventArgs args)
		{
			Window avalonWnd = (Window)target;
			ConsentDecisions result;
			try
			{
				TrustHostInfo hi = new TrustHostInfo();
				hi.ParentWindow = avalonWnd;
				hi.UIContext = TrustConsentContext.Runtime;

				ApplicationSecurityDescription asd = new ApplicationSecurityDescription();
				asd.Name = "Longhorn Trust Demo";
				asd.Publisher = "Thomas Hounsell";
				asd.Version = "v1.0";

				asd.RequestCollection = new TrustRequestCollection();
				asd.RequestCollection.Add(new TrustRequest(TrustType.Privacy, "Test permission"));

				TrustResult tr = new TrustResult();
				tr.InfoItemCollection = new TrustInfoItemCollection();
				tr.ScoreCollection = new TrustScoreCollection();
				tr.SettingCollection = new TrustSettingCollection();

				tr.InfoItemCollection.Add(new TrustInfoItem("Sample Info Item"));

				tr.ScoreCollection.Add(new TrustScore(ScoreRiskBucket.MaxRisk, TrustType.Privacy, "Sample Maximum Risk"));
				tr.ScoreCollection.Add(new TrustScore(ScoreRiskBucket.NoRisk, TrustType.Undefined, "Sample No Risk"));

				tr.SettingCollection.Add(new TrustSetting(ScoreRiskBucket.NoRisk, TrustType.Undefined, "Sample No Risk"));

				ConsentDialog cd = new ConsentDialog();
				result = cd.GetConsent(hi, asd, tr);
			}
			catch (Exception ex)
			{
				Console.WriteLine("\nMessage ---\n{0}", ex.Message);
				Console.WriteLine("\nHelpLink ---\n{0}", ex.HelpLink);
				Console.WriteLine("\nSource ---\n{0}", ex.Source);
				Console.WriteLine("\nStackTrace ---\n{0}", ex.StackTrace);
				Console.WriteLine("\nTargetSite ---\n{0}", ex.TargetSite);

				result = ConsentDecisions.None;
			}

			if (result == ConsentDecisions.All)
			{
				avalonWnd.Background = Paints.Green;
				avalonWnd.Text += " - Trust Granted!";
			}
			else
			{
				avalonWnd.Background = Paints.Red;
				avalonWnd.Text += " - Trust Refused!";
			}
		}
	}
}
