using System;

namespace LaunchTrustPages
{
	class Program
	{
		[STAThread]
		static void Main(string[] args)
		{
			MS.Internal.Privacy.TrustCenter tc = new MS.Internal.Privacy.TrustCenter();
			tc.Start();
		}
	}
}
