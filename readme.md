# Trust API Demonstrations for Longhorn
## Build 4033.main

This project has been tested and developed on Build 4033.main. This code will probably not run on very many other builds without modification, but equally, porting it to other builds shouldn't be too complex a task. In most cases, the changes will simply involve changing the Assemblies / Namespaces used. As per [Melcher's post on BetaArchive](https://www.betaarchive.com/forum/viewtopic.php?p=368897#p368897), swap System.Windows.Explorer.dll for ShellInterop.dll and MS.Internal.Privacy for System.Windows.Client.Privacy.Settings. You may also need to update the .NET Framework path in the `compile.bat` files.

### Building

These files include a `compile.bat` file which will generate an executable file for the build you are currently using.

## Launch Trust Center

This tool will launch the configuration page that allows you to set the trust levels on the current PC.

![Trust Preferences on Longhorn build 4033](https://i.imgur.com/zxFLc85.png)

## Launch Consent Dialog

This tool will trigger a basic consent dialog for the application on launch. If permissions are granted, the application window will turn green. Otherwise, the window will turn red.

![Consent Dialog in Longhorn build 4033](https://i.imgur.com/mbCJNzc.png)